/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"os"

	"gitee.com/bjf-fhe/apicat/source/nginx"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	_ "gitee.com/bjf-fhe/apicat/platform/aliyun"
	_ "gitee.com/bjf-fhe/apicat/platform/echo"
	_ "gitee.com/bjf-fhe/apicat/platform/nginx"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "apicat",
	Short: "OpenAPI日志分析程序",
	Long: `结合OpenAPI定义，分析Nginx访问日志，发现恶意访问，提高安全级别。
	
	使用方法：
		生成访问分析报告：	apicat report
		监听访问日志：		apicat watch

	具体使用方法请在命令后加--help获取`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) {

	// },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVarP(&rootConfig.Definition, "definition", "d", "", "OpenAPI definition")
	rootCmd.PersistentFlags().StringVarP(&rootConfig.UserName, "username", "u", "", "百家饭平台用户名/手机号/邮箱")

	rootCmd.PersistentFlags().StringVarP(&rootConfig.Server, "server", "s", "0", "选择使用那个服务器，可以输入'/'或者数字编号，数字编号对应OpenAPI服务器配置的编号，默认为0，也就是从OpenAPI文档中选择第一个")
	rootCmd.PersistentFlags().StringSliceVar(&rootConfig.ServerVariables, "server_variables", nil, "如果服务器地址有参数存在，可以通过该参数传入变量，输入形式为名称:值")
	rootCmd.PersistentFlags().StringSliceVar(&rootConfig.StaticFileTypes, "static_types", []string{".js", ".png", ".jpg", ".jpeg", ".js", ".css", ".html", "svg"}, "静态文件的后缀，列表中的文件类型会被认为是静态文件")
	rootCmd.PersistentFlags().StringVar(&rootConfig.SourceMode, "source", "nginx", "日志来源模式，支持nginx/aliyun")
	rootCmd.PersistentFlags().IntVar(&rootConfig.Debug, "log-level", int(logrus.WarnLevel), "日志级别，默认为3（Warning），可设置0（Panic）-1（Fatal）-2（Error）-4（Info）-5（Debug）-6（Trace）")
	rootCmd.PersistentFlags().StringVar(&rootConfig.TimeFormat, "time-formate", nginx.DefaultLocalTimeFormat, "默认时间格式，Nginx模式用于解析time_local,对time_iso8601无效")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	// rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
