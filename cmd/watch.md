# Watch 子命令运行参数说明

以下文字可通过运行`apicat watch --help`获得

```
对日志的实时生成条目进行即时增量读取，对其中的错误情况按配置汇报给下游，实现安全联动

Usage:
  apicat watch [flags]

Flags:
  -h, --help                   help for watch
      --nginx-cmd string       重启nginx的命令 (default "nginx -s reload")
      --nginx-count int        最低的错误次数 (default 2)
      --nginx-dest string      写入结果的目标文件 (default "/etc/nginx/conf.d/iptabls")
      --nginx-interval int     写入的最小间隔事件 (default 300)
      --nginx-level int        最低的错误级别 (default 10002)
      --nginx-workdir string   重启命令的执行目录
      --notify string          输出模式，nginx:输出为nginx配置 (default "nginx")

Global Flags:
  -c, --config string              Nginx配置文件
      --csv                        csv模式，指定后，将会以csv模式读取日志
  -d, --definition string          OpenAPI definition
  -f, --format string              Nginx配置文件中的log format名称 (default "main")
  -s, --server string              选择使用那个服务器，可以输入'/'或者数字编号，数字编号对应OpenAPI服务器配置的编号，默 认为0，也就是从OpenAPI文档中选择第一个 (default "0")
      --server_variables strings   如果服务器地址有参数存在，可以通过该参数传入变量，输入形式为名称:值
      --static_types strings       静态文件的后缀，列表中的文件类型会被认为是静态文件 (default [.js,.png,.jpg,.jpeg,.js,.css,.html,svg])
  -u, --username string            百家饭平台用户名/手机号/邮箱
```

## notify类型

目前watch命令支持对nginx的ip拦截配置支持

详见[这里](../notify/nginx/readme.md)