package utils

import (
	"bytes"
)

type BufCanBeNotified interface {
	Subscribe(c chan bool)
}

type BufWithNotify struct {
	buf       bytes.Buffer
	notifiers []chan bool
}

func (b *BufWithNotify) Subscribe(c chan bool) {
	b.notifiers = append(b.notifiers, c)
}

func (b *BufWithNotify) Write(p []byte) (n int, err error) {
	for _, c := range b.notifiers {
		if len(c) < cap(c) {
			c <- true
		}
	}
	return b.buf.Write(p)
}

func (b *BufWithNotify) Read(p []byte) (n int, err error) {
	return b.buf.Read(p)
}
