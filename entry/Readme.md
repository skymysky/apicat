## 错误级别说明

目前设置以下级别：



| 编号  | 程序显示               | 说明                                                         |
| ----- | ---------------------- | ------------------------------------------------------------ |
| 9999  | UnknownError           | 未知错误（内部错误，日常不会出现）                           |
| 10000 | Legal                  | 合法访问                                                     |
| 10001 | Illegal_StaticView     | Openapi文件中没有定义过的静态文件                            |
| 10002 | Illegal_UnknownUrl     | Openapi文件中没有定义过的非静态文件（危险级别较静态文件高）  |
| 10003 | Illegal_Method         | Openapi中定义过url，但是访问方法错误                         |
| 10004 | Illegal_EmptyArgs      | Openapi中定义过该url和对应的访问方法，但未提交任何参数       |
| 10005 | Illegal_UnexpectedArgs | Openapi中定义过该url和对应的方法，但访问提交的参数不符合定义中规定的参数范围 |
| 10006 | Illegal_DiffusedArgs   | Openapi中定义过该url和对应的方法，但访问提交的参数被判定为滥用 |

