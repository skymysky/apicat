package entry

import "fmt"

const (
	UnknownError EntryError = iota + 9999
	Legal
	Illegal_StaticView
	Illegal_UnknownUrl
	Illegal_Method
	Illegal_EmptyArgs
	Illegal_UnexpectedArgs
	Illegal_DiffusedArgs
)

type EntryError int

func (e EntryError) String() string {
	var description string
	switch e {
	case Legal:
		description = "Legal"
	case Illegal_Method:
		description = "Illegal_Method"
	case Illegal_StaticView:
		description = "Illegal_Static_View"
	case Illegal_EmptyArgs:
		description = "Illegal_Empty_Args"
	case Illegal_UnexpectedArgs:
		description = "Illegal_Unexpected_Args"
	case Illegal_DiffusedArgs:
		description = "Illegal_Unexpected_Args"
	case Illegal_UnknownUrl:
		description = "Unknown Url"
	case UnknownError:
		description = "Unknown Error"
	}
	return fmt.Sprintf("[%d]%s", e, description)
}
