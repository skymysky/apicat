package entry

import (
	"fmt"
	"testing"
)

func TestLogEntry(t *testing.T) {
	var e LogEntry
	e.Url = "/api/list.json?Limit=10&Self=true&Limit=20&Empty="
	e.SeperatePath()
	if e.Url != "/api/list.json" {
		t.Fail()
	}
	fmt.Println(e.Query.Get("Limit"))
	fmt.Println(e.Query["NotExisted"])
	fmt.Println(e.Query["Empty"])
}
