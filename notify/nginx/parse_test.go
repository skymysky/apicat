package nginx

import (
	"bytes"
	"fmt"
	"testing"
)

func TestParseExisted(t *testing.T) {
	var content = `# related-url:/test created-time:2022-11-30T07:26:03Z error-type:[10001]Illegal_Static_View error-count:12
deny 1.1.1.1;
# related-url:/test created-time:2022-11-30T07:26:03Z error-type:[10001]Illegal_Static_View error-count:12
deny 2.2.2.2;`
	results := parseReader(bytes.NewBufferString(content))
	fmt.Println(results)
	var wr bytes.Buffer
	var handler Handler
	err := handler.writeToWriter(results, &wr)
	if err != nil {
		t.Error(err)
		t.Fail()
	}
	var recreated = wr.String()
	if recreated != content {
		var minLength = len(recreated)
		if len(content) < len(recreated) {
			minLength = len(content)
		}
		for i := 0; i < minLength; i++ {
			if recreated[i] != content[i] {
				t.Error("not correct result in loc ", i, recreated[i:])
				t.Fail()
				return
			}
		}
	}
}

// # related-url:/test created-time:2022-11-30T07:26:03Z error-type:[10001]Illegal_Static_View
// deny 1.1.1.1;
// # related-url:/test created-time:2022-11-30T07:26:03Z error-type:[10001]Illegal_Static_View
// deny 2.2.2.2;

// # related-url:/test created-time:2022-11-30T07:26:03Z error-type:[10001]Illegal_Static_View
// deny 1.1.1.1;
// # related-url:/test created-time:2022-11-30T07:26:03Z error-type:[10001]Illegal_Static_View
// deny 2.2.2.2;
