# Nginx自动IP过滤配置

APICAT watch命令*支持*（进行中）自动配置nginx ip过滤，以便对日志分析中认为不正确的ip访问进行过滤。实现方式暂定为将不符合要求访问的来源IP配置为Deny（不允许访问）（更大精度可以是分url配置，TODO）。

## 启用方法

APICAT 的nginx过滤方式会自动维护一个包含所有ip deny列表的文件，如果需要启用，需要在nginx配置的http段或者server段增加以下配置：

```
include       /etc/nginx/conf.d/iptables;
```

路径是watch子命令配置的nginx输出日志文件路径，上述样例为默认路径，可根据自身情况修改

修改方式是通过--nginx-dest参数进行指定

## 重启Nginx

完成配置文件更新之后，默认会调用`nginx -s reload`使得nginx重新按配置初始化

如果是docker等其他情况，支持通过以下参数设置重启指令

`--nginx-workdir` 指定重启命令的执行目录
`--nginx-cmd` 指定重启命令的命令

例如如果是docker-compose执行的nginx命令

`--nginx-workdir`指向docker-compose.yaml文件的路径
`--nginx-cmd`设置为"'docker-compose exec <PROXY> nginx -s reload'"，其中<PROXY>替换为你的nginx service name，请注意因为该命令基本都是复杂命令，请使用引号将传入参数括起来

## 最小启动间隔

日志的检测和输出是实时探测的结果，但是在通常情况下，配置和重新加载不能根据错误实时执行，因此，设计为每隔一段时间周期执行，默认为5分钟。

这个参数可以通过`--nginx-interval`修改

## 配置设置错误最低次数

我们还可以设置最低的错误次数，只有超过该次数的访问者才会被认为是恶意访问者，加入不放行规则，这个判定是按用户来的，比如一个用户访问了两个错误地址，则次数为2。

这个设置通过--nginx-count我们可以设置，默认的配置为2

## 手动排除部分IP

TODO