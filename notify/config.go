package notify

type Config struct {
	MinLevel int
	MinCount int
}
