# 阿里云负载均衡自动规则创建

## 使用准备

### 准备AccessKeyId和AccessKeySecret

首先需要去阿里云创建AccessKeyId和AccessKeySecret（点击头像，选择AccessKey管理），创建AccessKey或者子账户AccessKey都可以。

建议创建子账户AccessKey，子账户可以控制权限。可以选择AliyunSLBFullAccess或者自创建规则，包含slb的以下操作：

- slb:AddAccessControlListEntry (写操作)
- slb:DescribeAccessControlListAttribute (读操作)
- slb:RemoveAccessControlListEntry (写操作)

### 找出需要配置的负载均衡的Region Id

点击负载均衡-SLB-实例管理，调整顶部的区域，直到出现需要配置的负载均衡出现在列表里，这时，url中slb后面的内容就是RegionID

比如

`https://slb.console.aliyun.com/slb/cn-beijing/slbs`

*阿里的同学能不能给个明显点的地方*

该参数通过`--aliyun-region-id`在运行时设置

### 创建访问控制策略组

使用本软件监听日志并创建阿里云负载均衡自动IP过滤策略需要首先创建一个访问控制策略组。

因为阿里云的访问策略组实现为IPv4/IPv6分离版本，因此，针对阿里云的自动规则创建目前支持配置两个策略组。

使用参数`--aliyun-dest`来设置IPv4的目标规则组

使用参数`--aliyun-dest-v6`来设置IPv6的目标规则组

