- access_all_success.log： 所有条目都为成功匹配的样例log
- access.log: 测试用的样例log
- docker-compose.yaml：本地启动nginx的测试docker-compose文件，如果本地安装有docker和docker-compose，可以启动一个本地的nginx用于测试
- log.conf: 测试用的log配置文件，用于测试程序的-c配置
- openapi.yaml: 测试用的openapi定义文件
