import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import { viteSingleFile } from 'vite-plugin-singlefile'
// import { createHtmlPlugin  } from 'vite-plugin-html';
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), viteSingleFile(),
  // createHtmlPlugin({
  //   minify: true,
  //   /**
  //    * After writing entry here, you will not need to add script tags in `index.html`, the original tags need to be deleted
  //    * @default src/main.ts
  //    */
  //   entry: 'src/main.js',
  //   /**
  //    * If you want to store `index.html` in the specified folder, you can modify it, otherwise no configuration is required
  //    * @default index.html
  //    */
  //   template: 'index.html',

  //   /**
  //    * Data that needs to be injected into the index.html ejs template
  //    */
  //   inject: {
  //     data: {
  //       title: 'index',
  //       injectScript: `<script src="./inject.js"></script>`,
  //     },
  //     tags: [
  //       {
  //         injectTo: 'body-prepend',
  //         tag: 'div',
  //         attrs: {
  //           id: 'tag',
  //         },
  //       },
  //     ],
  //   },
  // })
],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
})
