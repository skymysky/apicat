package nginx

import (
	"errors"
	"fmt"

	"gitee.com/bjf-fhe/apicat/entry"
	"gitee.com/bjf-fhe/apicat/notify"
	nginx_notifier "gitee.com/bjf-fhe/apicat/notify/nginx"
	"gitee.com/bjf-fhe/apicat/platform"
	"gitee.com/bjf-fhe/apicat/source"
	"github.com/spf13/pflag"
)

type Config struct {
	NotifierConfig nginx_notifier.Config
}

func (h *Config) AddFlags(set *pflag.FlagSet) {
}

func (h *Config) GetNotifier(ncfg *notify.Config) (platform.Notifier, error) {
	return h, nil
}

func (h *Config) GetSource(fileHandler platform.RootConfiger, tailMode bool) (source.Reader, error) {
	return nil, errors.New("no stumb source")
}

func (h *Config) Notify(ens map[string]*entry.LogEntry) error {
	fmt.Println(ens)
	return nil
}

func (h *Config) Check() error {
	return nil
}

func init() {
	platform.Register("echo", &Config{})
}
