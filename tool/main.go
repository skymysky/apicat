package main

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

var replaced = `////////////////template section////////////////
var analysis_result = {{ $ }};
//////////////////////////////////////` /////////////`

func main() {
	if len(os.Args) > 1 {
		file, err := os.Open(os.Args[1])
		if err == nil {
			bts, err := ioutil.ReadAll(file)
			if err == nil {
				matcher := regexp.MustCompile(`(?s)/{16}template section/{16}(.*)/{48}`)
				matched := matcher.ReplaceAllString(string(bts), replaced)
				matched = strings.ReplaceAll(matched, "`", "`+\"`\"+`")
				matched = `package result

var defaultTemplate = ` + "`" + matched + "`"
				ioutil.WriteFile(filepath.Join("result", "template.go"), []byte(matched), 0600)
			}
		}
	}

}
