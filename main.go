/*
Copyright © 2022 LiuMing@https://rongapi.cn <slamliu@qq.com>

*/
package main

import "gitee.com/bjf-fhe/apicat/cmd"

func main() {
	cmd.Execute()
}
