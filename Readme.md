基于OpenAPI的Nginx日志分析器，通过调取OpenAPI定义文档，对Nginx请求中的异常请求进行分析，以达到预警分析等目的。

# 原理

OpenAPI是一种用于定义API结构的规范，在Java里我们可以使用[swagger](http://rongapi.cn/docs/6_%E4%BD%BF%E7%94%A8%E6%A1%88%E4%BE%8B/java.html)进行自动生成。其他语言也可以（[Golang](http://rongapi.cn/docs/6_%E4%BD%BF%E7%94%A8%E6%A1%88%E4%BE%8B/golang.html)等）。通过这种对开发人员零成本的工具，我们可以高效的获取开放API服务的业务结构、合理输入及输出等描述信息。

结合这种描述信息以及实际发生的访问日志，我们就可以有效的对恶意访问进行筛选，比如常见的各式扫描访问（通过常见的管理页面、登录页面等扫描访问服务，达到获取服务漏洞的目的）。

# 安装方式

`go install gitee.com/bjf-fhe/apicat`

# 使用方式

使用go命令进行安装之后，运行apicat+子命令即可。子命令接收本文档的通用运行参数和一些特有参数的配置。特有参数配置参考子命令配置说明

## 子命令：report

`apicat report`

生成访问报告，运行命令后，会生成report.html，默认会调用系统命令进行打开

## 子命令：watch

`apicat watch`

实时监听访问日志，运行命令后，会监听日志文件变化，并根据规则实现对访问者ip的放行/阻止

子命令参数说明看[这里](./cmd/watch.md)

## 通用运行参数

```
Flags:
  -c, --config string       Nginx配置文件
  -d, --definition string   OpenAPI definition
  -f, --format string       Nginx配置文件中的log format名称 (default "main")
  -h, --help                help for apicat
  -u, --username string     百家饭平台用户名/手机号/邮箱
```

其中，-d可以输入：

- 数字，指[百家饭平台](https://rongapi.cn)的source id，输入数字的时候，需要提供-u参数指定平台用户名
- url，网络上的openapi定义url，如果是本地的swagger服务器，可以参考[这个指引](http://rongapi.cn/docs/6_%E4%BD%BF%E7%94%A8%E6%A1%88%E4%BE%8B/java.html)获得定义json
- 本地文件路径

-c是指定的nginx配置文件路径，主要要从该文件中获取log_format，以便匹配日志条目，如果不指定，会使用默认配置。默认配置为：
`log_format   main  '$remote_addr [$time_local] "$request"'`
如果不方便指定原始nginx配置，可以自行编辑包含以上内容的文本作为输入，仅包含该行即可

-f是指定的nginx配置中log_format的名称，log_format在nginx配置中，紧跟log_format是log_format的名称，通常为main，此时不需要单独指定该配置，如果配置中有修改，需要通过该参数指定。

## 指定日志路径

日志路径在运行参数的最后一位提供即可。

## 来源日志类型

- Nginx 日志
- Aliyun 导出CSV日志（从Aliyun日志库导出的csv类型的日志文件，不需要解压，直接将gz文件作为传入参数即可，导出日志时，请选择CSV类型，压缩方式选择默认的gz格式
）
- Aliyun 日志存储在线读取（支持从阿里云负载均衡配置的日志存储点直接获取在线日志条目）

# 最新状态

正在准备第一个初步功能版本，尽请期待

# 其他帮助

如果想了解更多关于openapi的功能，请访问[我们的网站](http://rongapi.cn/)，或在[官方论坛留言](https://bbs.csdn.net/forums/baijiafan)

# 修改本开源软件

本软件的UI部分采用vue编写，需要将其进行编译后生成成为golang template（更新result目录下的template.go中的defaultTemplate变量）。编译步骤如下:

```
  cd ui && yarn build
  go run ./tool .\ui\dist\index.html
```

