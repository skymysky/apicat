package source

import "gitee.com/bjf-fhe/apicat/entry"

type Reader interface {
	// Read() (*entry.LogEntry, error)
	Records() chan *entry.LogEntryResponse
}

type Parser interface {
	ParseLine(line string) (*entry.LogEntry, error)
}
