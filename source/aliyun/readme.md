# 数据源说明

本数据源是从阿里云的日志存储中获取负载均衡，使用时需要在负载均衡里配置日志存储

## API来源

按此说明[编写](https://next.api.aliyun.com/api/Sls/2020-12-30/PullData?sdkStyle=dara)

以下文档的说明似乎已经失效：
[https://www.aliyun.com/help/zh/log-service/latest/getlogs#t13238.html](https://www.aliyun.com/help/zh/log-service/latest/getlogs#t13238.html)

# 使用模式

使用时，请将logstore的完整url作为文件参数（即最后一个参数），比如（ali-test-project.cn-hangzhou.log.aliyuncs.com//logstores/{logstore}）。

## 如何获得这个url

首先查看[该指南](https://www.alibabacloud.com/help/zh/log-service/latest/endpoints)，获得你的日志存储Project的url