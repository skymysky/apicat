package csv

import (
	"encoding/csv"
	"fmt"
	"io"
	"strconv"
	"time"

	"gitee.com/bjf-fhe/apicat/entry"
	"gitee.com/bjf-fhe/apicat/utils"
	"github.com/sirupsen/logrus"
)

type Reader struct {
	titles   map[string]int
	reader   *csv.Reader
	notifier chan bool
}

func (r *Reader) NewEntry(rec []string) *entry.LogEntry {
	entry := new(entry.LogEntry)
	if rmi, ok := r.titles["request_method"]; ok && rmi < len(rec) {
		entry.Method = rec[rmi]
	}
	if rmi, ok := r.titles["request_uri"]; ok && rmi < len(rec) {
		entry.Url = rec[rmi]
	}
	if rmi, ok := r.titles["server_protocol"]; ok && rmi < len(rec) {
		entry.Protocol = rec[rmi]
	}
	if rmi, ok := r.titles["status"]; ok && rmi < len(rec) {
		entry.StatusCode, _ = strconv.Atoi(rec[rmi])
	}
	if cip, ok := r.titles["client_ip"]; ok && cip < len(rec) {
		entry.Client = rec[cip]
	}
	if tm, ok := r.titles["time"]; ok && tm < len(rec) {
		entry.Created, _ = time.Parse(time.RFC3339, rec[tm])
	}
	return entry
}

func (r *Reader) Records() chan *entry.LogEntryResponse {
	var c = make(chan *entry.LogEntryResponse)
	var err error
	go func() {
		for err == nil {
			var rec []string
			rec, err = r.reader.Read()
			if err == nil {
				c <- &entry.LogEntryResponse{
					Entry: r.NewEntry(rec),
				}
			} else if err == io.EOF {
				if r.notifier != nil {
					<-r.notifier
					fmt.Println("---")
				} else {
					logrus.Debug("used in waiting mode,waiting for 3 seconds")
					time.Sleep(3 * time.Second)
				}
				continue
			} else {
				c <- &entry.LogEntryResponse{
					Error: err,
				}
			}
		}
	}()

	return c
}

func NewReader(logFile io.Reader, tailMode bool) (*Reader, error) {
	reader := csv.NewReader(logFile)
	titles, err := reader.Read()
	if err == nil {
		tMap := make(map[string]int)
		for k, t := range titles {
			tMap[t] = k
		}
		var r = &Reader{reader: reader, titles: tMap}
		if n, ok := logFile.(utils.BufCanBeNotified); ok {
			r.notifier = make(chan bool, 1)
			n.Subscribe(r.notifier)
		}

		return r, nil
	}
	return nil, err
}
