package nginx

import (
	"io"

	"gitee.com/bjf-fhe/apicat/entry"
	"github.com/satyrius/gonx"
)

type Parser struct {
	parser     *gonx.Parser
	timeFormat string
}

func NewParser(nginxConf io.Reader, formatName string, timeFormat string) (*Parser, error) {
	parser, err := gonx.NewNginxParser(nginxConf, formatName)
	if err == nil {
		return &Parser{parser, timeFormat}, nil
	}
	return nil, err
}

func (p *Parser) ParseLine(line string) (*entry.LogEntry, error) {
	ne, err := p.parser.ParseString(line)
	if err == nil {
		entry := NewFromNginx(ne, p.timeFormat)
		return entry, nil
	}
	return nil, err
}
