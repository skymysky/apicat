package result

import (
	"os/exec"
)

func OpenResult() error {
	return exec.Command("xdg-open", "report.html").Run()
}
