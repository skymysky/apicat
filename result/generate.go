package result

import (
	"html/template"
	"os"
)

func GenerateReport(stat *Stat) error {
	var err error
	temp := template.New("generator")
	temp, err = temp.Parse(defaultTemplate)
	if err == nil {
		var f *os.File
		var dest = "report.html"
		f, err = os.OpenFile(dest, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0644)
		defer f.Close()
		if err == nil {
			err = temp.Execute(f, *stat)
		}
	}
	return err
}
