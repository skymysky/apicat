package result

import "gitee.com/bjf-fhe/apicat/entry"

type Stat struct {
	Brief       Brief
	ClientCount int
	Clients     map[string]int
	Detail      []*entry.LogEntry
}

func NewStat() *Stat {
	var stat Stat
	stat.Clients = make(map[string]int)
	return &stat
}

type Brief struct {
	LegalCount   int64
	IllegalCount int64
}

func (s *Brief) Total() int64 {
	return s.LegalCount + s.IllegalCount
}
