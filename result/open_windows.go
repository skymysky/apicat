package result

import (
	"os/exec"
)

func OpenResult() error {
	return exec.Command("cmd", "/c", "start", "report.html").Run()
}
