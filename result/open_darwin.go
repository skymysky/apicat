package result

import (
	"os/exec"
)

func OpenResult() error {
	return exec.Command("open", "report.html").Run()
}
