package result

import (
	"fmt"
	"testing"
)

func TestGenerate(t *testing.T) {
	var stat Stat
	stat.Brief.LegalCount = 4
	stat.Brief.IllegalCount = 20
	err := GenerateReport(&stat)
	fmt.Println(err)
}

func TestStart(t *testing.T) {
	OpenResult()
}
